import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import {
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  isLoggedIn
} from "react-native";

import { Login } from './screens/Login';
import { Signup } from './screens/Signup';
import { Home } from './screens/Home';
import { HomeLogin } from './screens/HomeLogin';
import { Profile } from './screens/Profile';
import { Category } from './screens/Category';

const Stack = createNativeStackNavigator();

export default function App() {
    return (
    // <Login/>
    // <NavigationContainer>
    //   <Stack.Navigator screenOptions={{headerShown: false}}>
    //   {isLoggedIn ? (
    //     <Stack.Group>
    //     <Stack.Screen name="Accueil" component={Home}/>
    //     <Stack.Screen name="Profile" component={Profile}/>
    //     <Stack.Screen name="Page Login" component={HomeLogin}/>
    //     <Stack.Screen name="Category" component={Category}/>
    //     </Stack.Group>
    //     ) : (
    //       <Stack.Group>
    //       <Stack.Screen name="Se connecter" component={Login}/>
    //       <Stack.Screen name="S'inscrire" component={Signup}/>
    //       </Stack.Group>
    //   )}
    //   </Stack.Navigator>
    // </NavigationContainer>
    <NavigationContainer>
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Accueil" component={Home}/>
      <Stack.Screen name="Profile" component={Profile}/>
      <Stack.Screen name="Page Login" component={HomeLogin}/>
      <Stack.Screen name="Category" component={Category}/>
      <Stack.Screen name="Se connecter" component={Login}/>
      <Stack.Screen name="S'inscrire" component={Signup}/>
    </Stack.Navigator>
  </NavigationContainer>
  );
}

// const styles = StyleSheet.create({
  
// });
