import React, { useEffect, useState } from "react";
import {  Image,  StyleSheet,  Text,  View,  TextInput,  TouchableOpacity, ToastAndroid, ScrollView, FlatList} from "react-native";
import { Ionicons, AntDesign, SimpleLineIcons } from '@expo/vector-icons'
import {LineChart} from "react-native-chart-kit";
import Moment from "moment";
import 'moment/locale/fr';
import axios from "axios";



export function HomeLogin({navigation}) {
const apiLink = "http://192.168.1.4:8000/";

  const [username, setUsername] = useState("");
  const [data, setData] = useState([]);

  const userId = 38;

  const urlApiSpend = apiLink + "api/spendings/";

  function axiosApiCall() {

    axios.get(urlApiSpend)
    .then(function (response) {
      setData(response.data);
      console.log(response.data);
    })
    .catch(function (error) {
  
      console.log(error);
    })
  
  }

  console.log(data);
  
    useEffect(() => {
      axiosApiCall();
    }, []);
  
  // data.sort((a, b) => (a.dateSpending > b.dateSpending) ? 1 : -1);

  // Moment.locale('fr');

  return (
    <View style={styles.container}>
      <View style={styles.mobileHeader}>

        <View style={styles.headerTop}>
          <AntDesign style={styles.iconProfile} name="user" size={24} color="black" onPress={() => navigation.navigate('Profile')}/>
            <Text style={styles.titleText}>Accueil</Text>
          <SimpleLineIcons style={styles.iconLogout} name="logout" size={24} onPress={() => navigation.navigate('Se connecter')}/>
        </View>
        
          <View style={styles.subHeader}>
            <Text style={styles.subTitleText}>Comptes</Text>
            <Text style={styles.subTitleText} onPress={() => navigation.navigate('Category')}>Catégories</Text>
          </View>

      </View>

      <Text style={styles.usernameText}>Bienvenue !</Text>

      <View style={styles.spendingBlock}>
        <Text style={styles.spendingText}>Vos dépenses</Text>

        <View style={styles.spendingContent}>
        <LineChart
          data={{
            labels: data.map(item => {
              return(
                Moment(item.dateSpending).format('D MMM Y')
              )
            }),
            datasets: [
              {
                data: data.map(item =>{
                  return(
                    item.spendingValue
                  )
                }),
              },
            ]
          }}
          width={2500}
          height={325}
          yAxisLabel={'€'}
          chartConfig={{
            backgroundColor: '#1cc910',
            backgroundGradientFromOpacity: 0.15,
            backgroundGradientFrom: '#000000',
            backgroundGradientToOpacity: 0.15,
            backgroundGradientTo: '#000000',
            decimalPlaces: 2,
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16,
            },
          }}
          style={{
            marginVertical: 8,
            borderRadius: 16,
            margin: 15
          }}
        />
        </View>
    </View>

      <View style={styles.spendingBlock}>
        <Text style={styles.spendingText}>Vos dépenses</Text>
        <View style={styles.spendingContent}>

        </View>
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#fff",
    alignItems: "center",
    marginTop: 25
  },

  mobileHeader: {
    backgroundColor: "#55976c",
    width: "100%",
  },

  headerTop:{
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: 50
  },

  titleText: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold"
  },

  iconProfile:{
    paddingRight: 125,
    color: "white"
  },

  iconLogout:{
    paddingLeft: 125,
    color: "white"
  },

  subHeader:{
    backgroundColor: "#e4e4f4",
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },

  subTitleText: {
    fontSize: 15,
    padding: 10,
  },

  usernameText: {
    fontSize: 20,
    margin: 25,
    textAlign: "center"
  },

  spendingBlock: {
    width: 375,
    height: 375,
    backgroundColor: "#7abe90",
    // opacity: 0.25,
    zIndex: 1,
    borderRadius: 25,
    marginBottom: 50
  },

  spendingText: {
    textAlign: "center",
    fontSize: 20,
    zIndex: 999,
    color: "black"
  },

  textInput: {
    backgroundColor: "white",
    width: "35%",
    textAlign: "center",
    alignSelf: "center"
  },

  spendingContent: {
    alignItems: "center",
    justifyContent: "center"
  }



});
