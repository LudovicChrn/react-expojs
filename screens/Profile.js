import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
  Button,
  Alert,
  ScrollView
} from "react-native";
import { Ionicons, AntDesign, SimpleLineIcons } from '@expo/vector-icons'


export function Profile({navigation}) {
  const [username, setUsername] = useState("");

  return (
    <View style={styles.container}>

        <View style={styles.mobileHeader}>
            <View style={styles.headerTop}>
                <AntDesign style={styles.iconBack} name="back" size={24} color="black" onPress={() => navigation.navigate('Page Login', {
              paramKey: username,
            })}/>
                <Text style={styles.titleText}>Profil</Text>
                <SimpleLineIcons style={styles.iconLogout} name="logout" size={24} onPress={() => navigation.navigate('Se connecter')}/>
            </View>

            <View style={styles.subHeader}>
                <Text style={styles.subTitleText}>Comptes</Text>
                <Text style={styles.subTitleText} onPress={() => navigation.navigate('Category')}>Catégories</Text>
            </View>
        </View>

        <ScrollView style={styles.scrollBar}>
            <View style={styles.spendingBlock}>
                <Text style={styles.spendingText}>Identifiant</Text>
                <TextInput style={styles.inputText} placeholder="Votre identifiant"/>
                <Text style={styles.spendingText}>Nom</Text>
                <TextInput style={styles.inputText} placeholder="Votre nom"/>
                <Text style={styles.spendingText}>Prénom</Text>
                <TextInput style={styles.inputText} placeholder="Votre prénom"/>
                <Text style={styles.spendingText}>Email</Text>
                <TextInput style={styles.inputText} placeholder="Votre email"/>
            </View>
        </ScrollView>
    </View>
  );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: "#fff",
        alignItems: "center",
        marginTop: 25
      },
    
      scrollBar: {
    
      },
    
      mobileHeader: {
        backgroundColor: "#55976c",
        width: "100%",
      },
    
      headerTop:{
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        height: 50
      },
    
      titleText: {
        fontSize: 15,
        color: "white",
        fontWeight: "bold"
      },
    
      iconBack:{
        paddingRight: 125,
        color: "white"
      },
    
      iconLogout:{
        paddingLeft: 125,
        color: "white"
      },
    
      subHeader:{
        backgroundColor: "#e4e4f4",
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
      },
    
      subTitleText: {
        fontSize: 15,
        padding: 10,
      },

      spendingBlock: {
        width: 375,
        height: 375,
        backgroundColor: "#7abe90",
        // opacity: 0.25,
        zIndex: 1,
        borderRadius: 25,
        marginBottom: 50,
        marginTop: 50,
        alignItems: "center",
        justifyContent: "center"
      },

      inputText: {
        textAlign: "center",
        fontSize: 18,
        width: "75%",
        borderRadius: 15,
        backgroundColor: "white"
      },
    
      spendingText: {
        textAlign: "center",
        fontSize: 20,
        marginTop: 15,
        zIndex: 999,
        color: "black"
      }
});
