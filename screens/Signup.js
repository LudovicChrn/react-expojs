import React, { useState } from "react";
import {  Image,  StyleSheet,  Text,  View,  TextInput,  TouchableOpacity,} from "react-native";
import { AntDesign } from '@expo/vector-icons'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export function Signup({navigation}) {
  const [name, setName] = useState("");
  const [nickname, setNickname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

return (
    <View style={styles.container}>

    <AntDesign style={styles.iconBack} name="back" size={32} color="black" onPress={() => navigation.navigate('Accueil')}/>

        <Text style={styles.titleText}>S'inscrire</Text>

        {/* <View style={styles.inputView}>
            <TextInput
            style={styles.TextInput}
            placeholder="Votre prénom"
            placeholderTextColor="#ffd766"
            onChangeText={(name) => setName(name)}
            />
        </View> */}

        {/* <View style={styles.inputView}>
            <TextInput
            style={styles.TextInput}
            placeholder="Votre nom"
            placeholderTextColor="#ffd766"
            onChangeText={(nickname) => setNickname(nickname)}
            />
        </View> */}

        <View style={styles.inputView}>
            <TextInput
            style={styles.TextInput}
            placeholder="Votre email"
            placeholderTextColor="#ffd766"
            onChangeText={(email) => setEmail(email)}
            />
        </View>

        <View style={styles.inputView}>
            <TextInput
            style={styles.TextInput}
            placeholder="Votre mot de passe"
            placeholderTextColor="#ffd766"
            onChangeText={(password) => setPassword(password)}
            />
        </View>

        <View style={styles.inputView}>
            <TextInput
            style={styles.TextInput}
            placeholder="Confirmer mot de passe"
            placeholderTextColor="#ffd766"
            onChangeText={(confirmPassword) => setConfirmPassword(confirmPassword)}
            />
        </View>

        <TouchableOpacity style={styles.validBtn}>
        <Text style={styles.validText} onPress={() => navigation.navigate('Se connecter')}>Valider</Text>
        </TouchableOpacity>
    </View>
);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e4e4f4",
    alignItems: "center",
    justifyContent: "center",
  },

  iconBack:{
    marginRight: 350,
    marginBottom: 25
  },

  titleText: {
    fontSize: 25,
    margin: 25,
    fontWeight: "bold"
  },

  inputView: {
    backgroundColor: "#7abe90",
    borderRadius: 30,
    borderColor: "#2c3e50",
    borderWidth: 2,
    width: "70%",
    height: 45,
    marginBottom: 5,
    alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 5,
  },

  validBtn: {
    width: "60%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#55976c",
    borderColor: "#2c3e50",
    borderWidth: 2,
  },

  validText: {
      color: '#f8b319',
  },
});
