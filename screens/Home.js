import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";

export function Home({navigation}) {

return (
    <View style={styles.container}>
        <View style={styles.titleContainer}> 
            <Text style={styles.titleText}>MyFlouZ</Text>
            <Text style={styles.subTitleText}>Pour une gestion pépouze</Text>
        </View>
        
      <Image
        style={styles.image}
        source={require("./../assets/img/img1.png")}
      />

      <TouchableOpacity style={styles.loginBtn} onPress={() => navigation.navigate('Se connecter')}>
        <Text style={styles.loginText}>Se connecter</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.registerBtn} onPress={() => navigation.navigate('S\'inscrire')}>
        <Text style={styles.registerText}>S'inscrire</Text>
      </TouchableOpacity>
    </View>
);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e4e4f4",
    alignItems: "center",
    justifyContent: "center",
  },

  titleContainer:{
    alignItems: "center",
    margin: 25
  },

  titleText: {
    fontSize: 25,
    fontWeight: "bold"
  },

  subTitleText: {
    fontSize: 15,
    fontWeight: "bold",
    fontStyle: "italic"
  },

  image: {
    width: 100,
    height: 100,
    marginBottom: 50,
  },


  loginBtn: {
    width: "60%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#55976c",
    borderColor: "#2c3e50",
    borderWidth: 2,
  },

  loginText: {
      color: '#f8b319',
      fontWeight: "bold"
  },

  registerBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
    backgroundColor: "#55976c",
    borderColor: "#2c3e50",
    borderWidth: 2,
  },

  registerText: {
      color:'#f8b319',
      fontWeight: "bold"
  }
});
