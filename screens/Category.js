import React, { useEffect, useState } from "react";
import { Text, StyleSheet, View, FlatList } from "react-native";
import CollapsibleView from "@eliav2/react-native-collapsible-view";
import { AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import axios from "axios";


export function Category({navigation}) {
  const apiLink = "http://192.168.1.4:8000/";

  const [data, setData] = useState([]);

  const userId = 38;

  const urlApiSpendings = apiLink + 'api/spendings/list/' + userId;



  function axiosApiCall() {

    axios.get(urlApiSpendings)
    .then(function (response) {
      setData(response.data);
      console.log(response.data);
    })
    .catch(function (error) {
  
      console.log(error);
    })
  
  }

  console.log(data);
  data.sort((a, b) => (a.spendingValue < b.spendingValue) ? 1 : -1);

  
    useEffect(() => {
      axiosApiCall();
    }, []);


  return (
    <View style={styles.container}>

      <View style={styles.mobileHeader}>
            <View style={styles.headerTop}>
                <AntDesign style={styles.iconBack} name="back" size={24} color="black" onPress={() => navigation.navigate('Page Login')}/>
                <Text style={styles.titleText}>Catégories</Text>
                <SimpleLineIcons style={styles.iconLogout} name="logout" size={24} onPress={() => navigation.navigate('Se connecter')}/>
            </View>

            <View style={styles.subHeader}>
                <Text style={styles.subTitleText}>Comptes</Text>
                <Text style={styles.subTitleText} onPress={() => navigation.navigate('Category')}>Catégories</Text>
            </View>
      </View>

      <View style={styles.categoryContent}>

          <FlatList data={data} keyExtractor={({ id }) => id.toString()} renderItem={({ item }) =>
              <CollapsibleView style={styles.categoryItem} title={<Text style={{ color: "#ffd766", fontSize: 17 }}>{item.spendingName}</Text>} arrowStyling={{ size: 20, rounded: true, thickness: 2.5, color: "#ffd766" }}>
                  <Text style={styles.categoryDescription}>{item.categoryDescription}</Text>
                  <Text style={styles.categoryDescription}>{item.spendingValue}€</Text>
                  <Text style={styles.categoryDescription}>{item.id}</Text>
              </CollapsibleView>
          }/>
      </View>

    </View>

  );
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      width: "100%",
      backgroundColor: "#fff",
      alignItems: "center",
      marginTop: 25
    },

    mobileHeader: {
      backgroundColor: "#55976c",
      width: "100%",
    },
  
    headerTop:{
      width: "100%",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      height: 50
    },
  
    titleText: {
      fontSize: 15,
      color: "white",
      fontWeight: "bold"
    },
  
    iconBack:{
      paddingRight: 125,
      color: "white"
    },
  
    iconLogout:{
      paddingLeft: 125,
      color: "white"
    },
  
    subHeader:{
      backgroundColor: "#e4e4f4",
      width: "100%",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center"
    },
  
    subTitleText: {
      fontSize: 15,
      padding: 10,
    },

    categoryContent:{
      width: "100%"
    },

    categoryItem:{
      width: "95%",
      marginTop: 5,
      marginBottom: 5,
      padding: 15,
      backgroundColor: "#7abe90",
      borderWidth: 2,
      borderColor: "#2c3e50",
      alignItems: "center",
      borderRadius: 20,
      marginLeft: 10,
      flexDirection: "column",
    },

    categoryDescription:{
      textAlign: "center",
      fontSize: 17
    }

})