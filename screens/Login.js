import React, { useState } from "react";
import {  StyleSheet, Text, View, TextInput, TouchableOpacity} from "react-native";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import { Formik } from 'formik';
import * as Yup from 'yup';


export function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const login = () => navigation.navigate("Page Login");

  /*const LoginSchema = Yup.object().shape({
    email: Yup.string().email('Email Invalide').required('Requis'),
    password: Yup.string()
      .min(8, 'Trop court!')
      .max(50, 'Trop long!')
      .required('Requis'),
  });*/

  return (
    <View style={styles.container}>
      <AntDesign style={styles.iconBack} name="back" size={32} color="black" onPress={() => navigation.navigate("Accueil")}/>

      <Text style={styles.titleText}>Se connecter</Text>

    <Formik initialValues={{ email: '', password: '' }} onSubmit={login} /*validationSchema={LoginSchema}*/>
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
          <View>
            <View style={styles.inputView}>
              <TextInput
                style={styles.TextInput}
                placeholder="Votre email"
                placeholderTextColor="#ffd766"
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
              />
            </View>
            {errors.email && touched.email ? (
            <Text style={styles.errorMessage}>{errors.email}</Text>
            ) : null}

            <View style={styles.inputView}>
              <TextInput style={styles.TextInput}
                secureTextEntry={true}
                placeholder="Votre mot de passe"
                placeholderTextColor="#ffd766"
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                value={values.password}
              />
            </View>
            {errors.password && touched.password ? (
            <Text style={styles.errorMessage}>{errors.password}</Text>
            ) : null}

            <TouchableOpacity>
              <Text style={styles.forgot_button}>Mot de passe oublié?</Text>
            </TouchableOpacity>

            <TouchableOpacity  style={styles.loginBtn} onPress={handleSubmit}>
              <Text style={styles.loginText} >Se connecter</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate("S'inscrire")}>
              <Text style={styles.forgot_button}>Pas de compte, inscrivez-vous?</Text>
            </TouchableOpacity>
          </View>
        )}
   </Formik>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e4e4f4",
    alignItems: "center",
    justifyContent: "center",
  },

  iconBack: {
    marginRight: 350,
    marginBottom: 25,
  },

  titleText: {
    fontSize: 25,
    margin: 25,
    fontWeight: "bold"
  },

  inputView: {
    backgroundColor: "#7abe90",
    borderRadius: 30,
    borderColor: "#2c3e50",
    borderWidth: 2,
    width: 250,
    height: 45,
    alignItems: "center",
    marginBottom: 5
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 5,
  },

  errorMessage:{
    textAlign: "center",
    color: "red",
    fontWeight: "bold",
    marginBottom: 15,
  },

  loginBtn: {
    width: 250,
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#55976c",
    borderColor: "#2c3e50",
    borderWidth: 2,
  },

  loginText: {
      color: '#f8b319',
      fontWeight: "bold"
  },

  forgot_button: {
    height: 30,
    marginBottom: 15,
    color: "#ff2000",
    textAlign: "center"
  },
});
